{-==== Игнатьев Максим, 11-302 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket [] = []
rocket list = if (snd found == minMass) then found : [] else list
      where found = (maximumBy (\x y -> compare (fst x `div` snd x) (fst y `div` snd y)) list)
            minMass = snd (minimumBy (\x y -> compare (snd x) (snd y)) list)

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters (x:xs) = case x of
          Rocket _ spaceships -> foldr (\x -> \y -> x:y) (orbiters spaceships) (orbiters xs)
          Cargo loads -> foldr (\x -> \y -> x:y) (orbitersFromLoad loads) (orbiters xs) 

orbitersFromLoad :: [Load a] -> [Load a]
orbitersFromLoad [] = []
orbitersFromLoad (x:xs) = case x of
          Orbiter _ -> x : orbitersFromLoad xs
          Probe _ -> orbitersFromLoad xs

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier (x:xs) = case x of
            Warp _ -> "Kirk" : finalFrontier xs
            BeamUp _ -> "Kirk" : finalFrontier xs
            IsDead _ -> "McCoy" : finalFrontier xs
            LiveLongAndProsper -> "Spock" : finalFrontier xs
            Fascinating -> "Spock" : finalFrontier xs
