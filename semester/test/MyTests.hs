-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests =   [testGroup "SemesterWork tests"
                [
                  testCase "eval (\\x.\\y.x) y = (\\y.a)"
                              $ show (eval (Apply (Lambda "x" (Lambda "z" (Var "x"))) (Var "z")))
                                  @?= "(\\z.aa)",
                  testCase "eval (\\x.x) y == y" $ eval (Apply (Lambda "x" (Var "x")) (Var "y")) @?= Var "y",
                  testCase "eval (\\x.\\y.x y) (\\z.z) == (\\y.(\\z.z) y)"
                                $ eval (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                               (Lambda "z" (Var "z")))
                                        @?= Lambda "y" (Apply (Lambda "z" (Var "z")) (Var "y")),
                  testCase "eval (\\x.(\\x.x) x) == (\\x.(\\x.x) x)"
                                $ show (eval (Lambda "x" (Apply (Lambda "x" (Var "x")) (Var "x"))))
                                    @?= "(\\x.(\\x.x) x)",
                  testCase "eval (\\x.\\a.x a) a = (\\a.b a)"
                                $ show (eval (Apply (Lambda "x"
                                            (Lambda "a" (Apply (Var "x") (Var "a")))) (Var "a")))
                                    @?= "(\\a.b a)"
                 ]
            ]
