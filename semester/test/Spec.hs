-- Do not change! It will be overwritten!
-- Use MyTests.hs to define your tests

import Test.Tasty (TestTree(..), defaultMainWithIngredients, testGroup)
import Test.Tasty.Ingredients.Basic (consoleTestReporter, listingTests)
import Test.Tasty.Runners.AntXML (antXMLRunner)

import MyTests (myTests)

main :: IO ()
main = defaultMainWithIngredients [antXMLRunner, consoleTestReporter, listingTests] tests

tests :: TestTree
tests = testGroup "Tests"
        [testGroup "User defined tests" myTests]
