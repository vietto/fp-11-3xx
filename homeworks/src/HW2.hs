-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval x = case x of
        Const x -> x
        Mult x y -> eval x * eval y
        Add x y -> eval x + eval y
        Sub x y -> eval x - eval y

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify x = case x of 
        (Const x) -> Const x
        (Mult x (Add y z)) -> simplify(Add (Mult (simplify x) (simplify y)) (Mult (simplify x) (simplify z)))
        (Mult (Add y z) x) -> simplify(Add (Mult (simplify y) (simplify x)) (Mult (simplify z) (simplify x)))
        (Mult x (Sub y z)) -> simplify(Sub (Mult (simplify x) (simplify y)) (Mult (simplify x) (simplify z)))
        (Mult (Sub y z) x) -> simplify(Sub (Mult (simplify y) (simplify x)) (Mult (simplify z) (simplify x)))
        (Add x y) -> Add (simplify x) (simplify y)
        (Sub x y) -> Sub (simplify x) (simplify y)
        (Mult x y) -> Mult (simplify x) (simplify y)